#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """

    lower = False
    for pos in range(len(first)):
        if pos < len(second):
            if first[pos].lower() < second[pos].lower():
                lower = True
                break
            if first[pos].lower() > second[pos].lower():
                break
    return lower


def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""
    lower: int = pos
    for i in range(pos + 1, len(words)):  # is the next argument (pos + 1) smaller than the pivot?
        if is_lower(words[i], words[lower]):  # booleano true or false (is_lower)
            lower = i
    return lower


def sort(words: list):
    """Return the list of words, ordered alphabetically"""

    for pivot in range(len(words)):  # pivot = argument 0
        lower = sort_pivot(words, pivot)  # function get_lower compares the arguments to find the smallest
        if lower != pivot:  # the smallest is not argument 0
            words[lower], words[pivot] = words[pivot], words[lower]     # changes the smallest argument for the pivot
    return words


def show(words: list):
    """Show words on screen, using print()"""

    for word in words:
        print(word, end=' ')
    print()


def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()

